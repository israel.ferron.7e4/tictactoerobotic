// Copyright 2000-2021 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
import androidx.compose.material.MaterialTheme
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application




@Composable
@Preview
fun App() {
    var board by remember { mutableStateOf(Board()) }
    if (board.mode == ""){
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ){
            Box(
                modifier = Modifier.width(200.dp).height(80.dp).padding(top = 10.dp).border(1.dp, Color.Black)
                    .background(color = Color(0xFFFF5733)).clickable {

                        board.setModeType("PvsM")
                        val aux = board
                        board = Board()
                        board = aux

                    }) {
                Text(
                    text = "Player VS Machine",
                    modifier = Modifier.align(Alignment.Center),
                    textAlign = TextAlign.Center,
                    color = Color.Black,
                    fontSize = 25.sp,
                )
            }
            Box(
                modifier = Modifier.width(200.dp).height(80.dp).padding(top = 20.dp).border(1.dp, Color.Black)
                    .background(color = Color(0xFFFF5733)).clickable {

                        board.setModeType("PvsP")
                        val aux = board
                        board = Board()
                        board = aux
                    }) {
                Text(
                    text = "Player VS Player",
                    modifier = Modifier.align(Alignment.Center),
                    textAlign = TextAlign.Center,
                    color = Color.Black,
                    fontSize = 25.sp,
                )
            }
        }
    }else {
        if (board.beginGame){
            board.connectToServer()
        }
        MaterialTheme {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                var text = ""
                if (board.currentPlayer) {
                    text = "Turn de X"
                } else {
                    text = "Turn de 0"
                }

                if (board.hasEnd) {
                    if (board.winner == "") {
                        text = "EL joc ha acaba" +
                                "t en empat"
                    } else {
                        text = "Jugador " + board.winner + " ha guanyat"
                    }
                }
                Text(
                    text = text,
                    fontSize = 30.sp,
                )
                board.board.forEachIndexed { y, row ->
                    Row() {
                        row.forEachIndexed { x, cell ->
                            val text = when (cell) {
                                true -> "X"
                                false -> "0"
                                else -> " "
                            }

                            if (!board.hasEnd) {
                                when (text) {
                                    "X" -> {
                                        Box(
                                            modifier = Modifier.width(80.dp).height(80.dp).border(1.dp, Color.Black)
                                                .background(color = Color(0xFF6BDC36)).clickable {
                                                board.playAt(x, y)
                                                val aux = board
                                                board = Board()
                                                board = aux
                                            }) {
                                            Text(
                                                text,
                                                modifier = Modifier.align(Alignment.Center),
                                                textAlign = TextAlign.Center,
                                                fontSize = 25.sp,
                                            )
                                        }
                                    }
                                    "0" -> {
                                        Box(
                                            modifier = Modifier.width(80.dp).height(80.dp).border(1.dp, Color.Black)
                                                .background(color = Color(0xFFDC5436)).clickable {
                                                board.playAt(x, y)
                                                val aux = board
                                                board = Board()
                                                board = aux
                                            }) {
                                            Text(
                                                text,
                                                modifier = Modifier.align(Alignment.Center),
                                                textAlign = TextAlign.Center,
                                                fontSize = 25.sp,
                                            )
                                        }
                                    }
                                    " " -> {
                                        Box(
                                            modifier = Modifier.width(80.dp).height(80.dp).border(1.dp, Color.Black)
                                                .background(color = Color(0xFFDAD5C7)).clickable {
                                                board.playAt(x, y)
                                                val aux = board
                                                board = Board()
                                                board = aux
                                            }) {
                                            Text(
                                                text,
                                                modifier = Modifier.align(Alignment.Center),
                                                textAlign = TextAlign.Center,
                                                fontSize = 25.sp,
                                            )
                                        }
                                    }
                                }

                            } else {
                                when (text) {
                                    "X" -> {
                                        Box(
                                            modifier = Modifier.width(80.dp).height(80.dp).border(1.dp, Color.Black)
                                                .background(color = Color(0xFFDC5436))
                                        ) {
                                            Text(
                                                text,
                                                modifier = Modifier.align(Alignment.Center),
                                                textAlign = TextAlign.Center,
                                                fontSize = 25.sp,
                                            )
                                        }
                                    }
                                    "0" -> {
                                        Box(
                                            modifier = Modifier.width(80.dp).height(80.dp).border(1.dp, Color.Black)
                                                .background(color = Color(0xFF6BDC36))
                                        ) {
                                            Text(
                                                text,
                                                modifier = Modifier.align(Alignment.Center),
                                                textAlign = TextAlign.Center,
                                                fontSize = 25.sp,
                                            )
                                        }
                                    }

                                    " " -> {
                                        Box(
                                            modifier = Modifier.width(80.dp).height(80.dp).border(1.dp, Color.Black)
                                                .background(color = Color(0xFFDAD5C7))
                                        ) {
                                            Text(
                                                text,
                                                modifier = Modifier.align(Alignment.Center),
                                                textAlign = TextAlign.Center,
                                                fontSize = 25.sp,
                                            )
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                if (board.hasEnd) {
                    Box(
                        modifier = Modifier.width(180.dp).height(40.dp).padding(top = 10.dp).border(1.dp, Color.Green)
                            .background(color = Color(0xFFEFF1A0)).clickable {

                            val aux = board
                            board = Board()
                            board = aux
                            board.cleanBoard()
                        }) {
                        Text(
                            text = "Reiniciar",
                            modifier = Modifier.align(Alignment.Center),
                            textAlign = TextAlign.Center,
                            color = Color.Black,
                            fontSize = 25.sp,
                        )
                    }
                }
            }
        }
    }
}


fun main() = application {
    Window(onCloseRequest = ::exitApplication) {
        App()
    }
}
