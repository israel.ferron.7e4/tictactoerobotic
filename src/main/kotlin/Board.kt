import java.io.BufferedInputStream
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.ServerSocket
import java.net.Socket


data class Board(var board : List<MutableList<Boolean?>> = List(3){MutableList(3){null}}, var currentPlayer : Boolean = true, var winner : String = "", var hasEnd : Boolean = false, var beginGame : Boolean = true, var mode: String = "") {
    private var socket : Socket? = null
    var server : ServerSocket? = null



    fun moveTo(x: Int, y: Int, player: Boolean) {
//        writer.write((message + '\n').toByteArray(Charset.defaultCharset()))

        val out = PrintWriter(socket?.getOutputStream(), true)
        val bis = BufferedInputStream(socket?.getInputStream())
        val isr = InputStreamReader(bis, "US-ASCII")
        var playerN = 0
        var end = 0
        if (player){
            playerN = 1
        }

        if(hasEnd){
            end = 1
        }

        out.println(listOf(x,y,playerN,end))
        out.flush()

        var c: Int
        while (isr.read().also { c = it } != 10) {
        }
//        println(player)
//        println(x)
//        println(y)




    }
    fun playAt(x: Int, y: Int){
        if(board[y][x]==null) {
            board[y][x] = currentPlayer
            currentPlayer = !currentPlayer
            beginGame = false
        }
        when (mode) {
            "PvsM" -> {
                comproveWinner()
                moveTo(x,y,currentPlayer)

                if (!hasEnd){
                    randomMove()
                }
            }
            "PvsP" -> {
                comproveWinner()
                moveTo(x, y, currentPlayer)
            }
        }

    }

    fun randomMove(){
        val x = (0..2).random()
        var y = (0..2).random()
        if(board[y][x]==null) {
            board[y][x] = currentPlayer
            currentPlayer = !currentPlayer
            comproveWinner()
            moveTo(x, y, currentPlayer)
        }else{
            randomMove()
        }
    }

    fun comproveEnd(){
        var auxBol = true
        board.forEachIndexed{ x, row ->
            row.forEachIndexed { y, cell ->
                if (cell == null){
                    auxBol = false
                }
            }
        }
        hasEnd = auxBol

    }

    fun cleanBoard(){
        closeConnection()
        socket = null
        board =  List(3){MutableList(3){null}}
        winner = ""
        hasEnd = false
        currentPlayer = true
        beginGame = true
        mode = ""
    }

    fun comproveWinner(){
        if (board[0][0] == true && board[0][1] == true && board[0][2] == true) winner = "X"
        else if (board[1][0] == true && board[1][1] == true && board[1][2] == true) winner = "X"
        else if (board[2][0] == true && board[2][1] == true && board[2][2] == true) winner = "X"
        else if (board[0][0] == true && board[1][0] == true && board[2][0] == true) winner = "X"
        else if (board[0][1] == true && board[1][1] == true && board[2][1] == true) winner = "X"
        else if (board[0][2] == true && board[1][2] == true && board[2][2] == true) winner = "X"
        else if (board[0][0] == true && board[1][1] == true && board[2][2] == true) winner = "X"
        else if (board[0][2] == true && board[1][1] == true && board[2][0] == true) winner = "X"
        else if (board[0][0] == false && board[0][1] == false && board[0][2] == false) winner = "0"
        else if (board[1][0] == false && board[1][1] == false && board[1][2] == false) winner = "0"
        else if (board[2][0] == false && board[2][1] == false && board[2][2] == false) winner = "0"
        else if (board[0][0] == false && board[1][0] == false && board[2][0] == false) winner = "0"
        else if (board[0][1] == false && board[1][1] == false && board[2][1] == false) winner = "0"
        else if (board[0][2] == false && board[1][2] == false && board[2][2] == false) winner = "0"
        else if (board[0][0] == false && board[1][1] == false && board[2][2] == false) winner = "0"
        else if (board[0][2] == false && board[1][1] == false && board[2][0] == false) winner = "0"

        if (winner != ""){
            hasEnd = true
        }else{
            comproveEnd()
        }
    }

    fun connectToServer(){
        server?.close()
        server = ServerSocket(29999);
        println("Server is running on port: ${server?.localPort}")
        println("Waiting for connection...")
        while (socket == null){
            socket = server?.accept()
        }
        println("Client connected: ${socket?.inetAddress?.hostAddress}")
    }

    fun closeConnection() {
        println("${socket?.inetAddress?.hostAddress} closed connection.")
        socket?.close()

    }

    fun setModeType(modeSelected : String){
        mode = modeSelected
    }




}